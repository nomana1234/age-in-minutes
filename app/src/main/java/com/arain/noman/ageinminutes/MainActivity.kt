package com.arain.noman.ageinminutes

import android.app.DatePickerDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // final Button button = findViewById(R.id.btnDatePicker);
        val datePickerBtn = findViewById<Button>(R.id.btnDatePicker)

        datePickerBtn.setOnClickListener { view ->
            clickDatePicker(view)
        }

    }

    fun clickDatePicker(view: View) {
        val selectedDateView : TextView = findViewById(R.id.tvSelectedDate) as TextView
        val selectedDateInMin : TextView = findViewById(R.id.tvSelectedDateInMinutes) as TextView

        val myCalendar = Calendar.getInstance();
        val year = myCalendar.get(Calendar.YEAR);
        val month = myCalendar.get(Calendar.MONTH)
        val day = myCalendar.get(Calendar.DAY_OF_MONTH)



        val dpd = DatePickerDialog(this,
            DatePickerDialog.OnDateSetListener {
                view, year, monthOfYear, dayOfMonth ->

                selectedDateView.text = "${monthOfYear + 1}/${dayOfMonth}/${year}"

                val selectedDate = "${dayOfMonth}/${monthOfYear + 1}/${year}"

                val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
                val theDate = sdf.parse(selectedDate)
                val selectedDateToMinutes = theDate.time / 60000
                val currentDate = sdf.parse(sdf.format(System.currentTimeMillis()))
                val currentDateToMinutes = currentDate.time / 60000
                val differenceInMinutes = (currentDateToMinutes - selectedDateToMinutes)
                selectedDateInMin.text = differenceInMinutes.toString();

            }, year, month, day)
        dpd.datePicker.setMaxDate(Date().time - 86400000)
        dpd.show()
    }

    fun getMinutes(date: Date) {

    }

}